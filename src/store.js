import {createStore ,applyMiddleware} from 'redux';
import rootReducer from './reducers/index';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';

const logger = createLogger();
const middleware = applyMiddleware(thunk,logger);

const store = createStore(rootReducer,middleware);
export default store;