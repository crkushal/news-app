export const LoadNewsReducer = (state = [],action) => {
   switch(action.type){
      case 'LOAD_NEWS_SUCCESS':
         return action.news;
      default:
         return state;
   }
}