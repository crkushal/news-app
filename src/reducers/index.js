import {combineReducers} from 'redux';
import {LoadNewsReducer} from './LoadNewsReducer';

const rootReducer = combineReducers({news:LoadNewsReducer});
export default rootReducer;