class LoadApi{
   static getNews(){
      let apiKey = `4e9f5d53e00e4992999ec071f7b5a867`;
      let apiUrl = `https://newsapi.org/v2/everything?sources=espn&apiKey=4e9f5d53e00e4992999ec071f7b5a867`;
      return fetch(apiUrl)
      .then(response => {return response.json()})
      .catch(error =>{return error})
   }
}
export default LoadApi;