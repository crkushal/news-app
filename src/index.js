import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import store from './store';
import {BrowserRouter as Router ,Route, Switch ,Link} from 'react-router-dom';
import Home from './components/Home';
import News from './components/News';
import './App';

ReactDOM.render(
   <Provider store = {store}>
      <Router>
         <div>
            <div className="header"> 
               <div className="container">
                  <div className="row">
                     <div className="col-md-6 col-md-offset-3">
                        <div className="heading">
                           <Link to="/">
                              News
                           </Link>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <Switch>
               <Route  exact path="/" component={App}/>
               <Route exact  path="/" component={Home}/>
               <Route exact path="/news/:index" component={News}/>
            </Switch>
         </div>   
      </Router> 
   </Provider>,
document.getElementById('root'));
registerServiceWorker();
