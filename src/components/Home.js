import React, { Component } from 'react';
import {connect} from 'react-redux';
import { selectNews } from '../reducers/LoadNewsReducer';
import '../App.css';
import {Link } from 'react-router-dom';
import {RiseLoader} from 'react-spinners';

@connect(mapStateToProps)
class Home extends Component {

  constructor(props){
    super(props);
  }

  showNews =() =>{
    if (this.props.news.length !== 0) {
      let news = this.props.news;
      let result = news.articles;
      let data = result.map((val,index) =>{
        if(val.urlToImage !== null){
          return(
            <Link to={{pathname:'/news/'+index , state:{data:val}}} >
              <li key={index}>
                <div className="card animated fadeIn">
                  <img src={val.urlToImage} className="headline-image" alt="image"/>
                  <p className="news-headline">{val.title}</p>
                </div>
              </li>
            </Link>
          );
        }
      });
      return data;
    }
    else{
      return(
        <div className='sweet-loading'>
          <RiseLoader		
            color={'#c33646'} 
          />
        </div>
      );
    }
  }

  render() {
    return (
      <React.Fragment>
          <div className="cards">
            <ul>
              {this.showNews()}
            </ul>
          </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) =>{
  return{
    news:state.news
  }
}

