import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class News extends Component {

  showSingleNews = () =>{
    const newsData = this.props.history.location.state.data;
    console.log(newsData)
    if (newsData !== undefined) {
      return(
        <React.Fragment>
          <li>
            <div className="card animated fadeIn">
              <img src={newsData.urlToImage} className="headline-image" alt="image"/>
              <h4>
                {newsData.description}
              </h4>
              <h2>{newsData.publishedAt}</h2>
            </div>
          </li>
        </React.Fragment>
      );
    }
    else{
      return(
        <div>
          <h1>
            Loading....
          </h1>
        </div>
      );
    }
  }
  
  render() {
    return (
      <div className="content">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              <ul>
                <div className="cards">
                  {this.showSingleNews()}
                </div>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default News;
