import LoadApi from '../class/LoadApi';

export const loadNews = () =>{
  return function (dispatch){
     return LoadApi.getNews()
     .then(news =>{
        dispatch(loadNewsSuccess(news))
     })
     .catch(error =>{throw(error)});
  }
}
function loadNewsSuccess(news) {  
   return {type: 'LOAD_NEWS_SUCCESS', news};
}