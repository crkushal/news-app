import React, { Component } from 'react';
import {loadNews} from './actions/actionCreators';
import {connect} from 'react-redux';
import Home from './components/Home';
import {Link ,Route} from 'react-router-dom';
import News from './components/News';
import './App.css';

@connect({loadNews})
class App extends Component {

  constructor(props){
    super(props);
    this.props.loadNews();
  }

  render() {
    return (
      <div className="content">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              <div>
                <Route exact path="/" component={ Home } />
                <Route exact  path="/news" component={ News } />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

